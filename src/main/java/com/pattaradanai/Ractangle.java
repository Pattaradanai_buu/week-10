package com.pattaradanai;

public class Ractangle extends Shape{
    private double width;
    private double height;
    public  Ractangle(int width, double height) {
        super("Ractangle");
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return this.getName() + " width:" +this.width + " height:" +this.height;
    }

    @Override
    public double calArea() {
        return this.width * this.height;
    }

    @Override
    public double calPerimeter() {
        return this.width*2 + this.height*2;
    }
}
